<?php
namespace Tests\Unit\Sainsburys\Crawler;

use Prophecy\Argument;
use Sainsburys\Crawler\CrawlerInterface;
use Sainsburys\Crawler\Handler\ResponseHandlerInterface;
use Sainsburys\Crawler\PageCrawler;
use Sainsburys\Crawler\Response\ResponseInterface;
use Sainsburys\Http\ClientInterface;

class PageCrawlerTest extends \PHPUnit_Framework_TestCase {

	public function testCrawlerImplementsTheRightInterface() {
		$pageCrawler = new PageCrawler();
		$this->assertInstanceOf(CrawlerInterface::class, $pageCrawler);
	}

	/**
	 * @covers \Sainsburys\Crawler\PageCrawler::setResponseHandler
	 * @covers \Sainsburys\Crawler\PageCrawler::setClient
	 * @covers \Sainsburys\Crawler\PageCrawler::setUrl
	 * @covers \Sainsburys\Crawler\PageCrawler::crawl
	 */
	public function testCrawlerCallsTheClientWithTheRightUrlAndTheHandler() {
		$dummyUrl = 'http://www.google.com';

		$client = $this->prophesize(ClientInterface::class);
		$client->get($dummyUrl)->shouldBeCalled();

		$responseHandler = $this->prophesize(ResponseHandlerInterface::class);
		$responseHandler->handle(Argument::type(ResponseInterface::class))->shouldBeCalled();

		$pageCrawler = new PageCrawler();
		$pageCrawler->setResponseHandler($responseHandler->reveal());
		$pageCrawler->setClient($client->reveal());
		$pageCrawler->setUrl($dummyUrl);

		$pageCrawler->crawl();
	}


	public function testCrawlerCreatesTheResponseObjectWithTheContent() {
		$dummyUrl = 'http://www.google.com';
		$dummyText = 'bla bla bla';

		$client = $this->prophesize(ClientInterface::class);
		$client->get($dummyUrl)->shouldBeCalled()->willReturn($dummyText);

		$response = $this->prophesize(ResponseInterface::class);
		$response->setContent($dummyText)->shouldBeCalled();

		$responseHandler = $this->prophesize(ResponseHandlerInterface::class);
		$responseHandler->handle(Argument::type(ResponseInterface::class))->shouldBeCalled();

		$pageCrawler = new PageCrawler();
		$pageCrawler->setResponseHandler($responseHandler->reveal());
		$pageCrawler->setClient($client->reveal());
		$pageCrawler->setUrl($dummyUrl);

		$pageCrawler->crawl($response->reveal());
	}
}