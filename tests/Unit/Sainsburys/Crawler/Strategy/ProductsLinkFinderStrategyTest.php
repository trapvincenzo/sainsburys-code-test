<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy;

use Sainsburys\Crawler\Strategy\FinderStrategyInterface;
use Sainsburys\Crawler\Strategy\ProductsLinkFinderStrategy;


class ProductsLinkFinderStrategyTest extends \PHPUnit_Framework_TestCase {

	public function testProductsFinderStrategyImplementsTheRightInterface() {
		$strategy = new ProductsLinkFinderStrategy();

		$this->assertInstanceOf(FinderStrategyInterface::class, $strategy);
	}

	/**
	 * @covers \Sainsburys\Crawler\Strategy\ProductsFinderStrategy::find
	 */
	public function testProductsFinderReturnsEmptyArrayWithInvalidContent() {
		$content = '<div >Wrong one</div>';

		$strategy = new ProductsLinkFinderStrategy();
		$result = $strategy->find($content);

		$this->assertCount(0, $result);
	}

	/**
	 * @covers \Sainsburys\Crawler\Strategy\ProductsFinderStrategy::find
	 */
	public function testProductsFinderFoundRightData() {
		$content = file_get_contents("./tests/Fixtures/list.txt");

		$strategy = new ProductsLinkFinderStrategy();
		$result = $strategy->find($content);

		$this->assertCount(2, $result);
		
		$this->assertEquals("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html", $result[0]);
		$this->assertEquals("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-loose-300g.html", $result[1]);
	}
}