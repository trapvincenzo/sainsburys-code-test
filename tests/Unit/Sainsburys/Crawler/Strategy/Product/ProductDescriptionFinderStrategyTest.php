<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy\Product;

use Sainsburys\Crawler\Strategy\Product\ProductDescriptionFinderStrategy;
use Tests\Unit\Sainsburys\Crawler\Strategy\Product\TestTrait\ProductFieldFinderStrategy;

class ProductDescriptionFinderStrategyTest extends \PHPUnit_Framework_TestCase {
	use ProductFieldFinderStrategy;

	public function getText() {
		return "Apricots";
	}

	public function getStrategy() {
		return new ProductDescriptionFinderStrategy();
	}
}