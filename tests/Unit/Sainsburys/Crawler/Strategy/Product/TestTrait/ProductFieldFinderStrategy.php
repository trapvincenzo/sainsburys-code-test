<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy\Product\TestTrait;

use Sainsburys\Crawler\Strategy\FinderStrategyInterface;

trait ProductFieldFinderStrategy {

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getStrategy() {
		throw new \Exception('Method not implemented');
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getText() {
		throw new \Exception('Method not implemented');
	}

	/**
	 * @return string
	 */
	public function getEmpty() {
		return '';
	}


	public function testStrategyImplementsTheRightInterface() {
		$strategy = $this->getStrategy();

		$this->assertInstanceOf(FinderStrategyInterface::class, $strategy);
	}

	public function testStrategyReturnsEmptyResultWithInvalidContent() {
		$content = '<div >Wrong one</div>';
		
		$strategy = $this->getStrategy();
		$result = $strategy->find($content);

		$this->assertEquals($this->getEmpty(), $result);
	}

	public function testStrategyFoundRightData() {
		$content = file_get_contents("./tests/Fixtures/product.txt");

		$strategy = $this->getStrategy();
		$result = $strategy->find($content);

		$this->assertEquals($this->getText(), $result);
	}
}