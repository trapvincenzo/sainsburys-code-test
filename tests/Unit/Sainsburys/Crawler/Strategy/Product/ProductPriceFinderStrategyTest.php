<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy\Product;

use Sainsburys\Crawler\Strategy\Product\ProductPriceFinderStrategy;
use Tests\Unit\Sainsburys\Crawler\Strategy\Product\TestTrait\ProductFieldFinderStrategy;

class ProductPriceFinderStrategyTest extends \PHPUnit_Framework_TestCase {
	use ProductFieldFinderStrategy;


	public function getText() {
		return '3.50';
	}

	public function getStrategy() {
		return new ProductPriceFinderStrategy();
	}
}