<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy\Product;

use Sainsburys\Crawler\Strategy\Product\ProductPageSizeFinderStrategy;
use Tests\Unit\Sainsburys\Crawler\Strategy\Product\TestTrait\ProductFieldFinderStrategy;

class ProductPageSizeFinderStrategyTest extends \PHPUnit_Framework_TestCase {
	use ProductFieldFinderStrategy;

	public function getText() {
		return "9.57kb";
	}

	public function getEmpty() {
		return "0.02kb";
	}

	public function getStrategy() {
		return new ProductPageSizeFinderStrategy();
	}
}