<?php

namespace Tests\Unit\Sainsburys\Crawler\Strategy\Product;

use Sainsburys\Crawler\Strategy\Product\ProductTitleFinderStrategy;
use Tests\Unit\Sainsburys\Crawler\Strategy\Product\TestTrait\ProductFieldFinderStrategy;

class ProductTitleFinderStrategyTest extends \PHPUnit_Framework_TestCase {
	use ProductFieldFinderStrategy;

	public function getText() {
		return "Sainsbury's Apricot Ripe &amp; Ready x5";
	}

	public function getStrategy() {
		return new ProductTitleFinderStrategy();
	}
}