<?php
namespace Tests\Unit\Sainsburys\Crawler\Handler;

use Prophecy\Argument;
use Sainsburys\Container\ContainerInterface;
use Sainsburys\Crawler\Handler\ListHandler;
use Sainsburys\Crawler\Handler\ProductHandler;
use Sainsburys\Crawler\Handler\ResponseHandlerInterface;
use Sainsburys\Model\Product;
use Sainsburys\Crawler\PageCrawler;
use Sainsburys\Crawler\Response\ResponseInterface;
use Sainsburys\Crawler\Strategy\ProductsLinkFinderStrategy;

class ListHandlerTest extends \PHPUnit_Framework_TestCase {

	public function testListHandlerImplementsTheRightInterface() {
		$handler = new ListHandler();

		$this->assertInstanceOf(ResponseHandlerInterface::class, $handler);
	}

	public function testReturnsAListOfLinks() {
		$reponse = $this->prophesize(ResponseInterface::class);

		$links = [
			'http://www.google.it/1',
			'http://www.google.it/2',
		];

		$productsFinder = $this->prophesize(ProductsLinkFinderStrategy::class);
		$productsFinder->find(Argument::any())->willReturn($links);

		$product = $this->prophesize(Product::class);

		$productHandler = $this->prophesize(ProductHandler::class);

		$pageCrawler = $this->prophesize(PageCrawler::class);
		$pageCrawler->setUrl($links[0])->shouldBeCalled();
		$pageCrawler->setUrl($links[1])->shouldBeCalled();
		$pageCrawler->setResponseHandler($productHandler->reveal())->shouldBeCalled();
		$pageCrawler->crawl()->shouldBeCalled()->willReturn($product);


		$container = $this->prophesize(ContainerInterface::class);
		$container->get('page_crawler')->willReturn($pageCrawler->reveal());
		$container->get('product_handler')->willReturn($productHandler->reveal());

		$handler = new ListHandler($productsFinder->reveal());
		$handler->setContainer($container->reveal());

		$objects = $handler->handle($reponse->reveal());

		$this->assertCount(2, $objects);
		$this->assertInstanceOf(Product::class, $objects[0]);
	}
}