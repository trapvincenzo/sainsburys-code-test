<?php


namespace Tests\Unit\Sainsburys\Crawler\Handler;


use Sainsburys\Crawler\Builder\ProductBuilder;
use Sainsburys\Crawler\Handler\ProductHandler;
use Sainsburys\Model\Product;
use Sainsburys\Crawler\Response\ResponseInterface;

class ProductHandlerTest extends \PHPUnit_Framework_TestCase {

	public function testHandlerReturnsAProductFromAContent() {
		$response = $this->prophesize(ResponseInterface::class);
		$response->getContent()->willReturn('');

		$product = $this->prophesize(Product::class);

		$productBuilder = $this->prophesize(ProductBuilder::class);
		$productBuilder->setRawContent('')->shouldBeCalled()->willReturn($productBuilder);
		$productBuilder->build()->shouldBeCalled()->willReturn($product->reveal());

		$handler = new ProductHandler($productBuilder->reveal());
		$this->assertInstanceOf(Product::class, $handler->handle($response->reveal()));
	}
}