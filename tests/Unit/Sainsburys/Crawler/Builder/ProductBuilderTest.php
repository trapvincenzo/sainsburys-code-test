<?php
namespace Tests\Unit\Sainsburys\Crawler\builder;

use Prophecy\Argument;
use Sainsburys\Crawler\Builder\ProductBuilder;
use Sainsburys\Model\Product;
use Sainsburys\Crawler\Strategy\FinderStrategyInterface;

class ProductBuilderTest extends \PHPUnit_Framework_TestCase {

	public function testProductHasBeenBuiltCorrectly() {
		$content = file_get_contents("./tests/Fixtures/product.txt");

		$builder = new ProductBuilder([
			'title' => $this->getStrategy('Title'),
			'price' => $this->getStrategy(1.80),
			'description' => $this->getStrategy('Description'),
			'size' => $this->getStrategy('1kb'),
		]);

		$builder->setRawContent($content);
		$product = $builder->build();
		
		$this->assertEquals($product->getTitle(), 'Title');
		$this->assertEquals($product->getPrice(), 1.80);
		$this->assertEquals($product->getDescription(), 'Description');
		$this->assertEquals($product->getSize(), '1kb');

		$this->assertInstanceOf(Product::class, $product);
	}

	private function getStrategy($value) {
		$strategy = $this->prophesize(FinderStrategyInterface::class);
		$strategy->find(Argument::any())->willReturn($value);

		return $strategy->reveal();
	}
}