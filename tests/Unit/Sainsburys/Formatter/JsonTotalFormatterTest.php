<?php
namespace Tests\Unit\Sainsburys\Formatter;

use Sainsburys\Formatter\Json\JsonTotalFormatter;

class JsonTotalFormatterTest extends \PHPUnit_Framework_TestCase {
	
	public function testJsonFormatter() {
		$object = new \stdClass();
		$object->total = "15.50";

		$expected = '{"total":15.50}';

		$content = json_encode($object);
		$content = (new JsonTotalFormatter())->format($content);

		$this->assertEquals($expected, $content);
	}
}