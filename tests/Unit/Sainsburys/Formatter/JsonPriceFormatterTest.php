<?php
namespace Tests\Unit\Sainsburys\Formatter;

use Sainsburys\Formatter\Json\JsonPriceFormatter;

class JsonPriceFormatterTest extends \PHPUnit_Framework_TestCase {
	
	public function testJsonFormatter() {
		$object = new \stdClass();
		$object->unit_price = "1.50";

		$expected = '{"unit_price":1.50}';

		$content = json_encode($object);
		$content = (new JsonPriceFormatter())->format($content);

		$this->assertEquals($expected, $content);
	}
}