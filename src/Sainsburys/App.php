<?php

namespace Sainsburys;

use Sainsburys\Container\Container;
use Sainsburys\Container\ContainerInterface;
use Sainsburys\Crawler\Builder\ProductBuilder;
use Sainsburys\Crawler\Handler\ListHandler;
use Sainsburys\Crawler\Handler\ProductHandler;
use Sainsburys\Crawler\PageCrawler;
use Sainsburys\Formatter\Json\JsonFormatter;
use Sainsburys\Formatter\Json\JsonPriceFormatter;
use Sainsburys\Formatter\Json\JsonTotalFormatter;
use Sainsburys\Http\FileGetContentsClient;

class App {

	/**
	 * @var ContainerInterface
	 */
	private $container;

	public function __construct() {
		$this->loadDependencies();
	}


	private function loadDependencies() {
		$container = new Container();

		$container['filegetcontents_client'] = function () {
			return new FileGetContentsClient();
		};

		$container['page_crawler'] = function ($c) {
			$crawler = new PageCrawler();
			$crawler->setClient($c['filegetcontents_client']);
			return $crawler;
		};

		$container['list_handler'] = function ($c) {
			$handler = new ListHandler();
			$handler->setContainer($c);

			return $handler;
		};

		$container['product_builder'] = function () {
			return new ProductBuilder();
		};

		$container['product_handler'] = function ($c) {
			return new ProductHandler($c['product_builder']);
		};

		$container['formatter'] = function () {
			$formatter = new JsonFormatter();
			$formatter->addPostFormatter(new JsonPriceFormatter());
			$formatter->addPostFormatter(new JsonTotalFormatter());
			return $formatter;
		};

		$this->container = $container;
	}

	/**
	 * @return ContainerInterface
	 */
	public function getContainer() {
		return $this->container;
	}
}