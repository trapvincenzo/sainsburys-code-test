<?php


namespace Sainsburys\Formatter\Json;


use Sainsburys\Formatter\FormatterInterface;

class JsonTotalFormatter implements FormatterInterface {

	/**
	 * @param $content
	 * @return mixed
	 */
	public function format($content) {
		$pattern = "/\"total\":(\".*?\")/si";
		$matches = [];

		preg_match_all($pattern, $content, $matches);

		foreach ($matches[1] as $match) {
			$price = str_replace('"', '', $match);
			$content = str_replace($match, $price, $content);
		}

		return $content;
	}
}