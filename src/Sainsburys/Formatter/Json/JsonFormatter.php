<?php


namespace Sainsburys\Formatter\Json;


use Sainsburys\Formatter\FormatterInterface;

class JsonFormatter implements FormatterInterface {

	/** @var FormatterInterface[] */
	private $formatters = [];

	/**
	 * @param $content
	 * @return string
	 */
	private function postFormat($content) {
		foreach ($this->formatters as $formatter) {
			$content = $formatter->format($content);
		}

		return $content;
	}

	/**
	 * @param FormatterInterface $formatter
	 */
	public function addPostFormatter(FormatterInterface $formatter) {
		$this->formatters[] = $formatter;
	}

	/**
	 * @param $content
	 * @return mixed
	 */
	public function format($content) {
		return $this->postFormat(json_encode($content));
	}
}