<?php


namespace Sainsburys\Formatter;


interface FormatterInterface {
	/**
	 * @param $content
	 * @return mixed
	 */
	public function format($content);
}