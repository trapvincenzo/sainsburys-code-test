<?php
namespace Sainsburys\Crawler\Response;

interface ResponseInterface {
	/**
	 * @param string $content
	 */
	public function setContent($content);

	/**
	 * @return string
	 */
	public function getContent();
}