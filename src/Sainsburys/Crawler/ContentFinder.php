<?php


namespace Sainsburys\Crawler;


use Sainsburys\Crawler\Strategy\FinderStrategyInterface;

class ContentFinder {

	/**
	 * @param string $content
	 * @param FinderStrategyInterface $strategy
	 * @return mixed
	 */
	public static function get($content, FinderStrategyInterface $strategy) {
		return $strategy->find($content);
	}
}