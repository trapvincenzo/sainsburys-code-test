<?php


namespace Sainsburys\Crawler\Builder;


use Sainsburys\Crawler\ContentFinder;
use Sainsburys\Model\Product;
use Sainsburys\Crawler\Strategy\FinderStrategyInterface;
use Sainsburys\Crawler\Strategy\Product\ProductDescriptionFinderStrategy;
use Sainsburys\Crawler\Strategy\Product\ProductPageSizeFinderStrategy;
use Sainsburys\Crawler\Strategy\Product\ProductPriceFinderStrategy;
use Sainsburys\Crawler\Strategy\Product\ProductTitleFinderStrategy;

class ProductBuilder {

	/**
	 * @var string
	 */
	private $rawContent;

	/**
	 * @var FinderStrategyInterface[]
	 */
	private $strategies = [];

	/**
	 * ProductBuilder constructor.
	 * @param null|array $strategies
	 */
	public function __construct($strategies = null) {
		if (is_null($strategies)) {
			$this->strategies = [
				'title' => new ProductTitleFinderStrategy(),
				'price' => new ProductPriceFinderStrategy(),
				'description' => new ProductDescriptionFinderStrategy(),
				'size' => new ProductPageSizeFinderStrategy(),
			];
		} else {
			$this->strategies = $strategies;
		}
	}

	/**
	 * @param string $content
	 * @return $this
	 */
	public function setRawContent($content) {
		$this->rawContent = $content;

		return $this;
	}

	/**
	 * @return Product
	 */
	public function build() {
		return new Product(
			$this->find('title'),
			$this->find('description'),
			$this->find('size'),
			$this->find('price')
		);
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	private function find($key) {
		return ContentFinder::get($this->rawContent, $this->strategies[$key]);
	}
}