<?php


namespace Sainsburys\Crawler\Handler;


use Sainsburys\Container\ContainerInterface;
use Sainsburys\Crawler\ContentFinder;
use Sainsburys\Crawler\PageCrawler;
use Sainsburys\Crawler\Response\ResponseInterface;
use Sainsburys\Crawler\Strategy\ProductsLinkFinderStrategy;

class ListHandler implements ResponseHandlerInterface {

	/**
	 * @var null|ProductsLinkFinderStrategy
	 */
	private $productsFinderStrategy = null;

	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * ListHandler constructor.
	 * @param null $productsFinderStrategy
	 */
	public function __construct($productsFinderStrategy = null) {
		if (!is_null($productsFinderStrategy)) {
			$this->productsFinderStrategy = $productsFinderStrategy;
		} else {
			$this->productsFinderStrategy = new ProductsLinkFinderStrategy();
		}
	}

	/**
	 * @param ContainerInterface $container
	 */
	public function setContainer(ContainerInterface $container) {
		$this->container = $container;
	}

	/**
	 * @param ResponseInterface $response
	 * @return mixed
	 */
	public function handle(ResponseInterface $response) {
		$content = $response->getContent();
		$links = ContentFinder::get($content, $this->productsFinderStrategy);
		$products = [];

		// We need now to crawl all the product pages
		$crawler = $this->getPageCrawler();
		$crawler->setResponseHandler($this->getProductHandler());

		foreach ($links as $link) {
			$crawler->setUrl($link);
			$products[] = $crawler->crawl();
		}

		return $products;
	}

	/**
	 * @return PageCrawler
	 */
	private function getPageCrawler() {
		return $this->container->get('page_crawler');
	}


	/**
	 * @return ProductHandler
	 */
	private function getProductHandler() {
		return $this->container->get('product_handler');
	}
}