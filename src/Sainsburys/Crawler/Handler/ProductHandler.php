<?php


namespace Sainsburys\Crawler\Handler;


use Sainsburys\Crawler\Builder\ProductBuilder;
use Sainsburys\Crawler\Response\ResponseInterface;

class ProductHandler implements ResponseHandlerInterface {

	/**
	 * @var ProductBuilder
	 */
	private $productBuilder;

	public function __construct($productBuilder = null) {
		if (!is_null($productBuilder)) {
			$this->productBuilder = $productBuilder;
		}
	}

	/**
	 * @param ResponseInterface $response
	 * @return mixed
	 */
	public function handle(ResponseInterface $response) {
		$content = $response->getContent();
		return $this->productBuilder->setRawContent($content)->build();
	}
}