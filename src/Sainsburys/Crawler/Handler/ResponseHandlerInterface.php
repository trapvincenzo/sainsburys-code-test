<?php


namespace Sainsburys\Crawler\Handler;


use Sainsburys\Crawler\Response\ResponseInterface;

interface ResponseHandlerInterface {
	/**
	 * @param ResponseInterface $response
	 * @return mixed
	 */
	public function handle(ResponseInterface $response);
}