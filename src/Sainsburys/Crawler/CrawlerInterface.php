<?php


namespace Sainsburys\Crawler;


use Sainsburys\Crawler\Handler\ResponseHandlerInterface;
use Sainsburys\Crawler\Response\ResponseInterface;
use Sainsburys\Http\ClientInterface;

interface CrawlerInterface {

	/**
	 * @param ClientInterface $client
	 */
	public function setClient(ClientInterface $client);

	/**
	 * @param ResponseHandlerInterface $handler
	 */
	public function setResponseHandler(ResponseHandlerInterface $handler);

	/**
	 * @return string
	 */
	public function getUrl();

	/**
	 * @param ResponseInterface $response
	 * @return mixed
	 */
	public function crawl(ResponseInterface $response = null);
}