<?php
namespace Sainsburys\Crawler\Strategy\Product;


class ProductPageSizeFinderStrategy extends AbstractProductFieldFinderStrategy {

	/**
	 * @param string $content
	 * @return string
	 */
	public function find($content) {
		return round(mb_strlen($content, '8bit') / 1024, 2) . 'kb';
	}
}