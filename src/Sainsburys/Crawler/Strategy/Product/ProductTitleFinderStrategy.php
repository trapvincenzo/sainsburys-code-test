<?php
namespace Sainsburys\Crawler\Strategy\Product;


class ProductTitleFinderStrategy extends AbstractProductFieldFinderStrategy {

	/**
	 * Pattern to get the title
	 * @var string
	 */
	protected $pattern = "/<h1>(.*?)<\/h1>/si";
}