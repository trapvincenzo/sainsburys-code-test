<?php
namespace Sainsburys\Crawler\Strategy\Product;


class ProductPriceFinderStrategy extends AbstractProductFieldFinderStrategy {

	/**
	 * Pattern to get the price per unit
	 * @var string
	 */
	protected $pattern = "/<p class=\"pricePerUnit\">.*?&pound;([0-9]*\.?[0-9]*).*?/si";
}