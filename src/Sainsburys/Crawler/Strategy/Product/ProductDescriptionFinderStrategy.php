<?php
namespace Sainsburys\Crawler\Strategy\Product;


class ProductDescriptionFinderStrategy extends AbstractProductFieldFinderStrategy {

	/**
	 * Pattern to get the description
	 * @var string
	 */
	protected $pattern = "/<div class=\"productText\">(.*?)<\/div>/si";
}