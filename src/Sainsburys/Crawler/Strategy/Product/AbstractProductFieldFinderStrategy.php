<?php


namespace Sainsburys\Crawler\Strategy\Product;


use Sainsburys\Crawler\Strategy\FinderStrategyInterface;

class AbstractProductFieldFinderStrategy implements FinderStrategyInterface {

	/**
	 * @var string
	 */
	protected $pattern;

	/**
	 * @param string $content
	 * @return mixed
	 */
	public function find($content) {
		$text = '';
		$matches = [];

		if (preg_match($this->pattern, $content, $matches)) {
			$text = trim(strip_tags($matches[1]));
		}

		return $text;
	}
}