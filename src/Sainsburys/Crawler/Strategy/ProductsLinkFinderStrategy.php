<?php


namespace Sainsburys\Crawler\Strategy;


class ProductsLinkFinderStrategy implements FinderStrategyInterface {

	/**
	 * Pattern to extract the links
	 * @var string
	 */
	private $pattern = "/<div class=\"productInfo\">.*?<a href=\"(.*?)\".*?<\/div>/si";

	/**
	 * @param string $content
	 * @return mixed
	 */
	public function find($content) {
		$matches = [];

		preg_match_all($this->pattern, $content, $matches);

		return $matches[1];

	}
}