<?php


namespace Sainsburys\Crawler\Strategy;


interface FinderStrategyInterface {
	/**
	 * @param string $content
	 * @return mixed
	 */
	public function find($content);
}