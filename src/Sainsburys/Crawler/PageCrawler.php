<?php
namespace Sainsburys\Crawler;

use Sainsburys\Crawler\Handler\ResponseHandlerInterface;
use Sainsburys\Crawler\Response\Response;
use Sainsburys\Crawler\Response\ResponseInterface;
use Sainsburys\Http\ClientInterface;

class PageCrawler implements CrawlerInterface {

	/**
	 * @var ResponseHandlerInterface
	 */
	private $responseHandler;

	/** @var ClientInterface */
	private $client;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @param ClientInterface $client
	 */
	public function setClient(ClientInterface $client) {
		$this->client = $client;
	}

	/**
	 * @param ResponseHandlerInterface $handler
	 */
	public function setResponseHandler(ResponseHandlerInterface $handler) {
		$this->responseHandler = $handler;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * @param ResponseInterface $response
	 * @return mixed
	 */
	public function crawl(ResponseInterface $response = null) {
		$response = $response ?: new Response();

		$content = $this->client->get($this->getUrl());
		$response->setContent($content);

		return $this->responseHandler->handle($response);
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}
}