<?php


namespace Sainsburys\Container;

use Pimple\Container as PimpleContainer;

class Container extends PimpleContainer implements ContainerInterface {

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key) {
		return $this[$key];
	}
}