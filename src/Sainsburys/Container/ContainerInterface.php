<?php


namespace Sainsburys\Container;


interface ContainerInterface {
	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get($key);
}