<?php

namespace Sainsburys\Http;

interface ClientInterface {
	/**
	 * @param string $url
	 * @param array $params
	 * @return mixed
	 */
	public function get($url, $params = null);
}