<?php


namespace Sainsburys\Http;


class FileGetContentsClient implements ClientInterface {

	/**
	 * @param string $url
	 * @param array $params
	 * @return mixed
	 */
	public function get($url, $params = null) {
		return file_get_contents($url);
	}
}