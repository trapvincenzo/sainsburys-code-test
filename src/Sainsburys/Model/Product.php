<?php
namespace Sainsburys\Model;

class Product implements \JsonSerializable {

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var string
	 */
	private $size;

	/**
	 * @var float
	 */
	private $unit_price;

	/**
	 * Product constructor.
	 * @param string $title
	 * @param string $description
	 * @param string $size
	 * @param float $price
	 */
	public function __construct($title, $description, $size, $price) {
		$this->title = $title;
		$this->description = $description;
		$this->size = $size;
		$this->unit_price = $price;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * @return float
	 */
	public function getPrice() {
		return $this->unit_price;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return get_object_vars($this);
	}
}