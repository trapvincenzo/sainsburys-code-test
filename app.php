<?php

require 'vendor/autoload.php';

use Sainsburys\App;
use Sainsburys\Crawler\Handler\ListHandler;
use Sainsburys\Crawler\PageCrawler;
use Sainsburys\Model\Product;

$productsListUrl = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';

/**
 * When the app class instance is created, all the services are loaded into the container
 * and are ready to be used when needed.
 */
$app = new App();
$container = $app->getContainer();

/** @var ListHandler $listHandler */
$listHandler = $container->get('list_handler');

/** @var PageCrawler $pageCrawler */
$pageCrawler = $container->get('page_crawler');
$pageCrawler->setUrl($productsListUrl);

// The handler is responsible to process the output of the products list
$pageCrawler->setResponseHandler($listHandler);

/** @var Product[] $products */
$products = $pageCrawler->crawl();

$total = 0.00;
/** @var Product $product */
foreach ($products as $product) {
	$total += $product->getPrice();
}

$response = new \stdClass();
$response->results = $products;
$response->total = number_format($total, 2);

print $container->get('formatter')->format($response);