#Install dependencies
The app requires composer to install the 3rd party dependencies and to load the app structure using PSR-4.
You can get it here: [https://getcomposer.org/download/](https://getcomposer.org/download/).

```bash
composer install
```

#Dependency injection
The app uses dependency injection via container (using Pimple library). The definitions of the services are in the
``Sainsbury\App::loadDependencies`` method.


#Handlers
The handlers are in charge to process the output received from the crawler http call.

###List handler
Manages the list of the products and finds the links.

###Product handler
Manages the product page and extracts the properties. responsible to manage the product page and extract the information.

#Run
```bash
php app.php
```

#Test
```bash
php vendor/bin/phpunit
```